import { Component } from '@angular/core';
import {WordFrequency} from './WordFrequency';

interface WordFrequencyAnalyzer {
  calculateHighestFrequency(): void;
  icalculateFrequencyForWord(text: string): void;
  calculateMostFrequentNWords(n: number): void;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements WordFrequencyAnalyzer {
  wordFreq: WordFrequency[] = [];
  contains = false;
  highestFrequency: number;

  freqForWordWord: string = 'een';
  freqForWordFreq: number;


  mostFreqWords: WordFrequency[] = [];

  text: string[] = ['Dit', 'is', 'En', 'voorbeeld', 'tekst', 'waar', 'de' , 'woorden', 'van', 'geteld', 'worden', 'dus',
    'ik', 'ga', 'een', 'paar', 'keer', 'dezelfde', 'woorden', 'gebruiken', 'om', 'aan', 'te', 'tonen', 'dat', 'di', 'werkt', 'ook', 'met', 'hoofdletters'];

  words: string[] = [];
  counts: number[] = [];

  constructor() {
    // Voor het vullen van de Array
    this.countWords(this.text);
    console.log(this.wordFreq);

    // Voor het bereken van de hoogste frequentie
    this.calculateHighestFrequency();

    // Voor het bereken van het gekozen woord
    this.icalculateFrequencyForWord(this.freqForWordWord);

    // Sort list met hoogste frequentie
    this.calculateMostFrequentNWords(3);
  }

  countWords(wordlist: string[]): void {
    wordlist.forEach((obj: string) => {
      this.contains = false;
      this.wordFreq.forEach((obj2: WordFrequency) => {
        if (obj2.word.toLowerCase() === obj.toLowerCase()) {
          obj2.frequency++;
          this.contains = true;
        }
      });
      if (this.contains === false){
        this.wordFreq.push(new WordFrequency(obj.toLowerCase(), 1));
      }
    });
  }


  calculateHighestFrequency(): void {
    const highestFrequency = Math.max(...this.wordFreq.map(o => o.frequency), 0);
    this.highestFrequency = highestFrequency;
  }

  icalculateFrequencyForWord(text: string): void {
    this.wordFreq.forEach((obj: WordFrequency) => {
        if (obj.word === text){
          this.freqForWordFreq = obj.frequency;
        }
    });
  }

  calculateMostFrequentNWords(n: number): void {
    this.wordFreq.sort((a, b) => (a.frequency < b.frequency) ? 1 : -1);
    this.mostFreqWords = this.wordFreq.slice(0, n);
  }

}
