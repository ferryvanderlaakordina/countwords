export class WordFrequency {
  public word: string;
  public frequency: number;

  constructor(word: string, frequency: number) {
    this.word = word;
    this.frequency = frequency;
  }
}
